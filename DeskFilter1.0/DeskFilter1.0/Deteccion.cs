﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AForge.Video.DirectShow;
using AForge.Video;
using Emgu.CV;
using Emgu.CV.Structure;
namespace DeskFilter1._0
{
    public partial class Deteccion : Form
    {
        public Deteccion()
        {
            InitializeComponent();
        }
        private bool HayDispositivos;
        private FilterInfoCollection MisDispositivos;
        private VideoCaptureDevice MiWebCam;
        private Random rnd = new Random();
        private void btn_Activar_Click(object sender, EventArgs e)
        {
            CerrarWebCam();
            int i = comboBox1.SelectedIndex;
            string Nombre = MisDispositivos[i].MonikerString;
            MiWebCam = new VideoCaptureDevice(Nombre);
            MiWebCam.NewFrame += new NewFrameEventHandler(capturar);
            MiWebCam.Start();
        }

        static readonly CascadeClassifier cascadeClass = new CascadeClassifier("haarcascade_frontalface_alt_tree.xml");

        private void Deteccion_Load(object sender, EventArgs e)
        {
            cargarDispositovos();

        }
        public void cargarDispositovos()
        {
            MisDispositivos = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (MisDispositivos.Count > 0)
            {
                HayDispositivos = true;
                for (int i = 0; i < MisDispositivos.Count; i++)
                {
                    comboBox1.Items.Add(MisDispositivos[i].Name.ToString());
                }
                comboBox1.Text = MisDispositivos[0].Name.ToString();
            }
            else
            {
                HayDispositivos = false;
            }
        }
        public void CerrarWebCam()
        {
            if (MiWebCam != null && MiWebCam.IsRunning)
            {
                MiWebCam.SignalToStop();
                MiWebCam = null;
            }
        }
        private void capturar(object sender, NewFrameEventArgs e)
        {
            Bitmap Bit = (Bitmap)e.Frame.Clone();
            Image<Bgr, byte> gray = Bit.ToImage<Bgr, byte>();
            Rectangle[] rectangles = cascadeClass.DetectMultiScale(gray, 1.2, 1);
            foreach(Rectangle rectangle in rectangles)
            {
                using (Graphics graphics = Graphics.FromImage(Bit))
                {
                    Color randomColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                    using (Pen pen = new Pen(randomColor, 5))
                    {
                        graphics.DrawRectangle(pen, rectangle);
                    }
                }
            }
            PhotoWF.Image = Bit;
        }
    }
}
