﻿
namespace DeskFilter1._0
{
    partial class Deteccion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btn_Activar = new System.Windows.Forms.Button();
            this.PhotoWF = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PhotoWF)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.btn_Activar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(862, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 611);
            this.panel1.TabIndex = 1;
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.comboBox1.ForeColor = System.Drawing.SystemColors.Window;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(9, 187);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(191, 29);
            this.comboBox1.TabIndex = 19;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 100);
            this.panel3.TabIndex = 17;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel7.ForeColor = System.Drawing.Color.White;
            this.panel7.Location = new System.Drawing.Point(19, 237);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(10, 37);
            this.panel7.TabIndex = 12;
            // 
            // btn_Activar
            // 
            this.btn_Activar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_Activar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Activar.FlatAppearance.BorderSize = 0;
            this.btn_Activar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_Activar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Activar.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_Activar.ForeColor = System.Drawing.Color.White;
            this.btn_Activar.Location = new System.Drawing.Point(19, 237);
            this.btn_Activar.Margin = new System.Windows.Forms.Padding(1);
            this.btn_Activar.Name = "btn_Activar";
            this.btn_Activar.Size = new System.Drawing.Size(172, 37);
            this.btn_Activar.TabIndex = 1;
            this.btn_Activar.Text = "Activar camara";
            this.btn_Activar.UseVisualStyleBackColor = false;
            this.btn_Activar.Click += new System.EventHandler(this.btn_Activar_Click);
            // 
            // PhotoWF
            // 
            this.PhotoWF.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PhotoWF.BackColor = System.Drawing.Color.Transparent;
            this.PhotoWF.Location = new System.Drawing.Point(3, 0);
            this.PhotoWF.Name = "PhotoWF";
            this.PhotoWF.Size = new System.Drawing.Size(862, 608);
            this.PhotoWF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PhotoWF.TabIndex = 2;
            this.PhotoWF.TabStop = false;
            // 
            // Deteccion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(85)))), ((int)(((byte)(169)))));
            this.ClientSize = new System.Drawing.Size(1062, 611);
            this.Controls.Add(this.PhotoWF);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Deteccion";
            this.Text = "Deteccion";
            this.Load += new System.EventHandler(this.Deteccion_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PhotoWF)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btn_Activar;
        private System.Windows.Forms.PictureBox PhotoWF;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}