﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
namespace DeskFilter1._0
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Btn_Cerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg,int wParam,int lParam);

        private void Main_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle,0x112,0xf012,0);
        }

        private void btn_off_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)//Inicio
        {
            Child(new Inicio());
        }

        private void btn_info_Click(object sender, EventArgs e)
        {
            if(PanelInfo.Visible)
                PanelInfo.Visible = false;
            else
                 PanelInfo.Visible = true;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe", "https://drive.google.com/file/d/1YBCYbUpAQfPfGS6pGAK6-oDHL1kH5HTQ/view?usp=sharing");
        }

        private void PanelMenu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (panelSubmenuC.Visible)
                panelSubmenuC.Visible = false;
            else
                panelSubmenuC.Visible = true;
        }

        private void btn_CPhoto_Click(object sender, EventArgs e)
        {
            panelSubmenuC.Visible = false;
            Child(new Foto());
        }

        private void btn_video_Click(object sender, EventArgs e)
        {
            panelSubmenuC.Visible = false;
            Child(new Video());
        }

        private void btn_off_MouseHover(object sender, EventArgs e)
        {
            btn_off.Image = Properties.Resources.power_2_64__1_;
        }

        private void btn_off_MouseLeave(object sender, EventArgs e)
        {
            btn_off.Image = Properties.Resources.power_2_64;
        }

        private void Child(object child)
        {
            if (this.PanelContenido.Controls.Count > 1)
                this.PanelContenido.Controls.RemoveAt(1);
            Form frm = child as Form;
            frm.TopLevel = false;
            frm.Dock = DockStyle.Fill;
            this.PanelContenido.Controls.Add(frm);
            this.PanelContenido.Tag = frm;
            frm.Show();
        }

        private void PanelContenido_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Main_Load(object sender, EventArgs e)
        {
            Child(new Inicio());
        }

        private void btn_Photo_Click(object sender, EventArgs e)
        {
            Child(new TomarFoto());
        }

        private void btn_Detection_Click(object sender, EventArgs e)
        {
            Child(new Deteccion());
        }
    }
}
