﻿
namespace DeskFilter1._0
{
    partial class Foto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Foto));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_save = new System.Windows.Forms.PictureBox();
            this.panelFilters = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btn_tintav = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btn_tintaA = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btn = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btn_negativo = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_icono = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btn_sepia = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_BW = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_filters = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btn_load = new System.Windows.Forms.Button();
            this.PhotoWF = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panelEdit = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_save)).BeginInit();
            this.panelFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PhotoWF)).BeginInit();
            this.panelEdit.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.btn_save);
            this.panel1.Controls.Add(this.panelFilters);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btn_filters);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.btn_load);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(862, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 611);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 100);
            this.panel3.TabIndex = 17;
            // 
            // btn_save
            // 
            this.btn_save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_save.Image = ((System.Drawing.Image)(resources.GetObject("btn_save.Image")));
            this.btn_save.Location = new System.Drawing.Point(77, 517);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(74, 70);
            this.btn_save.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_save.TabIndex = 16;
            this.btn_save.TabStop = false;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // panelFilters
            // 
            this.panelFilters.Controls.Add(this.panel11);
            this.panelFilters.Controls.Add(this.btn_tintav);
            this.panelFilters.Controls.Add(this.panel9);
            this.panelFilters.Controls.Add(this.btn_tintaA);
            this.panelFilters.Controls.Add(this.panel10);
            this.panelFilters.Controls.Add(this.btn);
            this.panelFilters.Controls.Add(this.panel8);
            this.panelFilters.Controls.Add(this.btn_negativo);
            this.panelFilters.Controls.Add(this.panel6);
            this.panelFilters.Controls.Add(this.btn_icono);
            this.panelFilters.Controls.Add(this.panel5);
            this.panelFilters.Controls.Add(this.btn_sepia);
            this.panelFilters.Controls.Add(this.panel4);
            this.panelFilters.Controls.Add(this.btn_BW);
            this.panelFilters.Location = new System.Drawing.Point(33, 212);
            this.panelFilters.Name = "panelFilters";
            this.panelFilters.Size = new System.Drawing.Size(160, 277);
            this.panelFilters.TabIndex = 15;
            this.panelFilters.Visible = false;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel11.ForeColor = System.Drawing.Color.White;
            this.panel11.Location = new System.Drawing.Point(3, 126);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(10, 37);
            this.panel11.TabIndex = 31;
            // 
            // btn_tintav
            // 
            this.btn_tintav.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_tintav.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_tintav.FlatAppearance.BorderSize = 0;
            this.btn_tintav.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_tintav.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_tintav.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_tintav.ForeColor = System.Drawing.Color.White;
            this.btn_tintav.Location = new System.Drawing.Point(3, 126);
            this.btn_tintav.Margin = new System.Windows.Forms.Padding(1);
            this.btn_tintav.Name = "btn_tintav";
            this.btn_tintav.Size = new System.Drawing.Size(157, 37);
            this.btn_tintav.TabIndex = 30;
            this.btn_tintav.Text = "Sepia frio";
            this.btn_tintav.UseVisualStyleBackColor = false;
            this.btn_tintav.Click += new System.EventHandler(this.btn_tintav_Click);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel9.ForeColor = System.Drawing.Color.White;
            this.panel9.Location = new System.Drawing.Point(3, 85);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(10, 37);
            this.panel9.TabIndex = 29;
            // 
            // btn_tintaA
            // 
            this.btn_tintaA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_tintaA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_tintaA.FlatAppearance.BorderSize = 0;
            this.btn_tintaA.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_tintaA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_tintaA.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_tintaA.ForeColor = System.Drawing.Color.White;
            this.btn_tintaA.Location = new System.Drawing.Point(3, 85);
            this.btn_tintaA.Margin = new System.Windows.Forms.Padding(1);
            this.btn_tintaA.Name = "btn_tintaA";
            this.btn_tintaA.Size = new System.Drawing.Size(157, 37);
            this.btn_tintaA.TabIndex = 28;
            this.btn_tintaA.Text = "Polaroid";
            this.btn_tintaA.UseVisualStyleBackColor = false;
            this.btn_tintaA.Click += new System.EventHandler(this.btn_tintaA_Click);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel10.ForeColor = System.Drawing.Color.White;
            this.panel10.Location = new System.Drawing.Point(3, 44);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(10, 37);
            this.panel10.TabIndex = 27;
            // 
            // btn
            // 
            this.btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn.FlatAppearance.BorderSize = 0;
            this.btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn.ForeColor = System.Drawing.Color.White;
            this.btn.Location = new System.Drawing.Point(3, 44);
            this.btn.Margin = new System.Windows.Forms.Padding(1);
            this.btn.Name = "btn";
            this.btn.Size = new System.Drawing.Size(157, 37);
            this.btn.TabIndex = 26;
            this.btn.Text = "Tinta roja";
            this.btn.UseVisualStyleBackColor = false;
            this.btn.Click += new System.EventHandler(this.Rojizo_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel8.ForeColor = System.Drawing.Color.White;
            this.panel8.Location = new System.Drawing.Point(3, 244);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(10, 37);
            this.panel8.TabIndex = 23;
            // 
            // btn_negativo
            // 
            this.btn_negativo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_negativo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_negativo.FlatAppearance.BorderSize = 0;
            this.btn_negativo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_negativo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_negativo.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_negativo.ForeColor = System.Drawing.Color.White;
            this.btn_negativo.Location = new System.Drawing.Point(3, 244);
            this.btn_negativo.Margin = new System.Windows.Forms.Padding(1);
            this.btn_negativo.Name = "btn_negativo";
            this.btn_negativo.Size = new System.Drawing.Size(157, 37);
            this.btn_negativo.TabIndex = 22;
            this.btn_negativo.Text = "Papel viejo";
            this.btn_negativo.UseVisualStyleBackColor = false;
            this.btn_negativo.Click += new System.EventHandler(this.btn_negativo_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel6.ForeColor = System.Drawing.Color.White;
            this.panel6.Location = new System.Drawing.Point(3, 203);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(10, 37);
            this.panel6.TabIndex = 21;
            // 
            // btn_icono
            // 
            this.btn_icono.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_icono.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_icono.FlatAppearance.BorderSize = 0;
            this.btn_icono.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_icono.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_icono.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_icono.ForeColor = System.Drawing.Color.White;
            this.btn_icono.Location = new System.Drawing.Point(3, 203);
            this.btn_icono.Margin = new System.Windows.Forms.Padding(1);
            this.btn_icono.Name = "btn_icono";
            this.btn_icono.Size = new System.Drawing.Size(157, 37);
            this.btn_icono.TabIndex = 20;
            this.btn_icono.Text = "Icono";
            this.btn_icono.UseVisualStyleBackColor = false;
            this.btn_icono.Click += new System.EventHandler(this.btn_icono_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel5.ForeColor = System.Drawing.Color.White;
            this.panel5.Location = new System.Drawing.Point(3, 162);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(10, 37);
            this.panel5.TabIndex = 19;
            // 
            // btn_sepia
            // 
            this.btn_sepia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_sepia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_sepia.FlatAppearance.BorderSize = 0;
            this.btn_sepia.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_sepia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sepia.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_sepia.ForeColor = System.Drawing.Color.White;
            this.btn_sepia.Location = new System.Drawing.Point(3, 162);
            this.btn_sepia.Margin = new System.Windows.Forms.Padding(1);
            this.btn_sepia.Name = "btn_sepia";
            this.btn_sepia.Size = new System.Drawing.Size(157, 37);
            this.btn_sepia.TabIndex = 18;
            this.btn_sepia.Text = "Sepia";
            this.btn_sepia.UseVisualStyleBackColor = false;
            this.btn_sepia.Click += new System.EventHandler(this.btn_sepia_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel4.ForeColor = System.Drawing.Color.White;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(10, 37);
            this.panel4.TabIndex = 17;
            // 
            // btn_BW
            // 
            this.btn_BW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_BW.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_BW.FlatAppearance.BorderSize = 0;
            this.btn_BW.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_BW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_BW.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_BW.ForeColor = System.Drawing.Color.White;
            this.btn_BW.Location = new System.Drawing.Point(3, 3);
            this.btn_BW.Margin = new System.Windows.Forms.Padding(1);
            this.btn_BW.Name = "btn_BW";
            this.btn_BW.Size = new System.Drawing.Size(157, 37);
            this.btn_BW.TabIndex = 16;
            this.btn_BW.Text = "Blanco y negro";
            this.btn_BW.UseVisualStyleBackColor = false;
            this.btn_BW.Click += new System.EventHandler(this.btn_BW_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(18, 171);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 37);
            this.panel2.TabIndex = 14;
            // 
            // btn_filters
            // 
            this.btn_filters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_filters.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_filters.FlatAppearance.BorderSize = 0;
            this.btn_filters.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_filters.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_filters.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_filters.ForeColor = System.Drawing.Color.White;
            this.btn_filters.Location = new System.Drawing.Point(18, 171);
            this.btn_filters.Margin = new System.Windows.Forms.Padding(1);
            this.btn_filters.Name = "btn_filters";
            this.btn_filters.Size = new System.Drawing.Size(172, 37);
            this.btn_filters.TabIndex = 13;
            this.btn_filters.Text = "Filtros";
            this.btn_filters.UseVisualStyleBackColor = false;
            this.btn_filters.Click += new System.EventHandler(this.btn_filters_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel7.ForeColor = System.Drawing.Color.White;
            this.panel7.Location = new System.Drawing.Point(18, 119);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(10, 37);
            this.panel7.TabIndex = 12;
            // 
            // btn_load
            // 
            this.btn_load.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_load.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_load.FlatAppearance.BorderSize = 0;
            this.btn_load.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_load.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_load.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_load.ForeColor = System.Drawing.Color.White;
            this.btn_load.Location = new System.Drawing.Point(18, 119);
            this.btn_load.Margin = new System.Windows.Forms.Padding(1);
            this.btn_load.Name = "btn_load";
            this.btn_load.Size = new System.Drawing.Size(172, 37);
            this.btn_load.TabIndex = 1;
            this.btn_load.Text = "Cargar imagen";
            this.btn_load.UseVisualStyleBackColor = false;
            this.btn_load.Click += new System.EventHandler(this.btn_Home_Click);
            // 
            // PhotoWF
            // 
            this.PhotoWF.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PhotoWF.BackColor = System.Drawing.Color.Transparent;
            this.PhotoWF.Location = new System.Drawing.Point(0, 0);
            this.PhotoWF.Name = "PhotoWF";
            this.PhotoWF.Size = new System.Drawing.Size(862, 608);
            this.PhotoWF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PhotoWF.TabIndex = 1;
            this.PhotoWF.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panelEdit
            // 
            this.panelEdit.Controls.Add(this.PhotoWF);
            this.panelEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEdit.Location = new System.Drawing.Point(0, 0);
            this.panelEdit.Name = "panelEdit";
            this.panelEdit.Size = new System.Drawing.Size(862, 611);
            this.panelEdit.TabIndex = 1;
            // 
            // Foto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(85)))), ((int)(((byte)(169)))));
            this.ClientSize = new System.Drawing.Size(1062, 611);
            this.Controls.Add(this.panelEdit);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Foto";
            this.Text = "Foto";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_save)).EndInit();
            this.panelFilters.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PhotoWF)).EndInit();
            this.panelEdit.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox PhotoWF;
        private System.Windows.Forms.Button btn_load;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_filters;
        private System.Windows.Forms.Panel panelFilters;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btn_BW;
        private System.Windows.Forms.PictureBox btn_save;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btn_negativo;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btn_icono;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btn_sepia;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panelEdit;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btn;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btn_tintav;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btn_tintaA;
    }
}