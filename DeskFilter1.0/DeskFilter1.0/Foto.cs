﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using static DeskFilter1._0.Filtros;

namespace DeskFilter1._0
{
    public partial class Foto : Form
    {
        public Foto()
        {
            InitializeComponent();
        }

        private void btn_Home_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string imagen = openFileDialog1.FileName;
                    PhotoWF.Image = System.Drawing.Image.FromFile(imagen);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("El archivo seleccionado no es un tipo de imagen válido");
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            SaveFileDialog Guardar = new SaveFileDialog();
            Guardar.Filter = "Bitmap Image (.bmp)|*.bmp|Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|Tiff Image (.tiff)|*.tiff|Wmf Image (.wmf)|*.wmf";
            Image Imagen = PhotoWF.Image;
            Guardar.ShowDialog();
            Imagen.Save(Guardar.FileName);
        }

        private void btn_filters_Click(object sender, EventArgs e)
        {
            if(PhotoWF.Image!=null)
            {
                if (panelFilters.Visible)
                    panelFilters.Visible = false;
                else
                    panelFilters.Visible = true;
            }
        }

        private void btn_BW_Click(object sender, EventArgs e)
        {
            Bitmap bmp = EscalaGrisesRGBW((Bitmap)PhotoWF.Image);
            PhotoWF.Image = bmp;
        }

        private void btn_sepia_Click(object sender, EventArgs e)
        {
            Bitmap bmp =Sepia((Bitmap)PhotoWF.Image);
            PhotoWF.Image = bmp;            
        }

        private void btn_negativo_Click(object sender, EventArgs e)
        {
            Bitmap bmp = PapelViejoRGBW((Bitmap)PhotoWF.Image);
            PhotoWF.Image = bmp;
        }

        private void Rojizo_Click(object sender, EventArgs e)
        {
            Bitmap bmp = Filtros.Rojizo((Bitmap)PhotoWF.Image);
            PhotoWF.Image = bmp;
        }

        private void btn_tintaA_Click(object sender, EventArgs e)
        {
            Bitmap bmp = PolaroidRGBW((Bitmap)PhotoWF.Image);
            PhotoWF.Image = bmp;
        }

        private void btn_tintav_Click(object sender, EventArgs e)
        {
            Bitmap bmp =ColdSepiaRGBW((Bitmap)PhotoWF.Image);
            PhotoWF.Image = bmp;
        }

        private void btn_icono_Click(object sender, EventArgs e)
        {
            Bitmap bmp = Filtros.Icono((Bitmap)PhotoWF.Image);
            PhotoWF.Image = bmp;
        }
    }
}
