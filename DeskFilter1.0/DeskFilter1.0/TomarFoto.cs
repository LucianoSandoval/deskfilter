﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AForge.Video.DirectShow;
using AForge.Video;
using Emgu.CV;
using Emgu.CV.Structure;
using static DeskFilter1._0.Filtros;
namespace DeskFilter1._0
{
    public partial class TomarFoto : Form
    {
        public TomarFoto()
        {
            InitializeComponent();
        }
        private bool HayDispositivos;
        private FilterInfoCollection MisDispositivos;
        private VideoCaptureDevice MiWebCam;
        private Random rnd = new Random();
        string filtro ="";
        private void btn_load_Click(object sender, EventArgs e)
        {
            CerrarWebCam();
            int i = comboBox1.SelectedIndex;
            string Nombre = MisDispositivos[i].MonikerString;
            MiWebCam = new VideoCaptureDevice(Nombre);
            MiWebCam.NewFrame += new NewFrameEventHandler(capturar);
            MiWebCam.Start();
        }
        public void cargarDispositovos()
        {
            MisDispositivos = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (MisDispositivos.Count > 0)
            {
                HayDispositivos = true;
                for (int i = 0; i < MisDispositivos.Count; i++)
                {
                    comboBox1.Items.Add(MisDispositivos[i].Name.ToString());
                }
                comboBox1.Text = MisDispositivos[0].Name.ToString();
            }
            else
            {
                HayDispositivos = false;
            }
        }
        public void CerrarWebCam()
        {
            if (MiWebCam != null && MiWebCam.IsRunning)
            {
                MiWebCam.SignalToStop();
                MiWebCam = null;
            }
        }
        private void capturar(object sender, NewFrameEventArgs e)
        {
            Bitmap Bit = (Bitmap)e.Frame.Clone();
          
            switch (filtro)
            {
                case "Sepia":
                    PhotoWF.Image = SepiaRGBW(Bit);
                    break;
                case "Escala de grises":
                    PhotoWF.Image = EscalaGrisesRGBW(Bit);
                    break;
                case "Sepia frio":
                    PhotoWF.Image = ColdSepiaRGBW(Bit);
                    break;
                case "Polaroid":
                    PhotoWF.Image = PolaroidRGBW(Bit);
                    break;
                case "Papel viejo":
                    PhotoWF.Image = PapelViejoRGBW(Bit);
                    break;
                default:
                    PhotoWF.Image = Bit;
                    break;
            }
        }
        private void TomarFoto_Load(object sender, EventArgs e)
        {
            cargarDispositovos();
            comboBox2.Items.Add("Polaroid");
            comboBox2.Items.Add("Papel viejo");
            comboBox2.Items.Add("Sepia");
            comboBox2.Items.Add("Sepia frio");
            comboBox2.Items.Add("Escala de grises");
            comboBox2.Items.Add("Sin filtro");
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            filtro = comboBox2.SelectedItem.ToString();
        }

        private void btn_filters_Click(object sender, EventArgs e)
        {
            CerrarWebCam();
            
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            SaveFileDialog Guardar = new SaveFileDialog();
            Guardar.Filter = "Bitmap Image (.bmp)|*.bmp|Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|Tiff Image (.tiff)|*.tiff|Wmf Image (.wmf)|*.wmf";
            Image Imagen = PhotoWF.Image;
            Guardar.ShowDialog();
            Imagen.Save(Guardar.FileName);
        }
    }
}
