﻿
namespace DeskFilter1._0
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.BarraTitulo = new System.Windows.Forms.Panel();
            this.btn_info = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Btn_Cerrar = new System.Windows.Forms.PictureBox();
            this.PanelMenu = new System.Windows.Forms.Panel();
            this.panelSubmenuC = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btn_video = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_CPhoto = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_Detection = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_off = new System.Windows.Forms.PictureBox();
            this.btn_Photo = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Home = new System.Windows.Forms.Button();
            this.PanelContenido = new System.Windows.Forms.Panel();
            this.PanelInfo = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.BarraTitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_info)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cerrar)).BeginInit();
            this.PanelMenu.SuspendLayout();
            this.panelSubmenuC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_off)).BeginInit();
            this.PanelContenido.SuspendLayout();
            this.PanelInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // BarraTitulo
            // 
            this.BarraTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.BarraTitulo.Controls.Add(this.btn_info);
            this.BarraTitulo.Controls.Add(this.pictureBox1);
            this.BarraTitulo.Controls.Add(this.Btn_Cerrar);
            this.BarraTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.BarraTitulo.Location = new System.Drawing.Point(0, 0);
            this.BarraTitulo.Name = "BarraTitulo";
            this.BarraTitulo.Size = new System.Drawing.Size(1300, 30);
            this.BarraTitulo.TabIndex = 0;
            this.BarraTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            // 
            // btn_info
            // 
            this.btn_info.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_info.Image = ((System.Drawing.Image)(resources.GetObject("btn_info.Image")));
            this.btn_info.Location = new System.Drawing.Point(1226, 0);
            this.btn_info.Name = "btn_info";
            this.btn_info.Size = new System.Drawing.Size(34, 31);
            this.btn_info.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_info.TabIndex = 2;
            this.btn_info.TabStop = false;
            this.btn_info.Click += new System.EventHandler(this.btn_info_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1195, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(25, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Btn_Cerrar
            // 
            this.Btn_Cerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Cerrar.Image = ((System.Drawing.Image)(resources.GetObject("Btn_Cerrar.Image")));
            this.Btn_Cerrar.Location = new System.Drawing.Point(1266, -1);
            this.Btn_Cerrar.Name = "Btn_Cerrar";
            this.Btn_Cerrar.Size = new System.Drawing.Size(34, 31);
            this.Btn_Cerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Btn_Cerrar.TabIndex = 0;
            this.Btn_Cerrar.TabStop = false;
            this.Btn_Cerrar.Click += new System.EventHandler(this.Btn_Cerrar_Click);
            // 
            // PanelMenu
            // 
            this.PanelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.PanelMenu.Controls.Add(this.panelSubmenuC);
            this.PanelMenu.Controls.Add(this.panel4);
            this.PanelMenu.Controls.Add(this.label2);
            this.PanelMenu.Controls.Add(this.button1);
            this.PanelMenu.Controls.Add(this.label1);
            this.PanelMenu.Controls.Add(this.panel3);
            this.PanelMenu.Controls.Add(this.btn_Detection);
            this.PanelMenu.Controls.Add(this.panel2);
            this.PanelMenu.Controls.Add(this.btn_off);
            this.PanelMenu.Controls.Add(this.btn_Photo);
            this.PanelMenu.Controls.Add(this.panel1);
            this.PanelMenu.Controls.Add(this.btn_Home);
            this.PanelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelMenu.Location = new System.Drawing.Point(0, 30);
            this.PanelMenu.Name = "PanelMenu";
            this.PanelMenu.Size = new System.Drawing.Size(222, 620);
            this.PanelMenu.TabIndex = 1;
            // 
            // panelSubmenuC
            // 
            this.panelSubmenuC.Controls.Add(this.panel7);
            this.panelSubmenuC.Controls.Add(this.btn_video);
            this.panelSubmenuC.Controls.Add(this.panel6);
            this.panelSubmenuC.Controls.Add(this.btn_CPhoto);
            this.panelSubmenuC.Location = new System.Drawing.Point(44, 366);
            this.panelSubmenuC.Name = "panelSubmenuC";
            this.panelSubmenuC.Size = new System.Drawing.Size(178, 81);
            this.panelSubmenuC.TabIndex = 12;
            this.panelSubmenuC.Visible = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel7.ForeColor = System.Drawing.Color.White;
            this.panel7.Location = new System.Drawing.Point(1, 40);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(10, 37);
            this.panel7.TabIndex = 11;
            // 
            // btn_video
            // 
            this.btn_video.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_video.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_video.FlatAppearance.BorderSize = 0;
            this.btn_video.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_video.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_video.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_video.ForeColor = System.Drawing.Color.White;
            this.btn_video.Location = new System.Drawing.Point(8, 40);
            this.btn_video.Margin = new System.Windows.Forms.Padding(1);
            this.btn_video.Name = "btn_video";
            this.btn_video.Size = new System.Drawing.Size(170, 36);
            this.btn_video.TabIndex = 10;
            this.btn_video.Text = "Video";
            this.btn_video.UseVisualStyleBackColor = false;
            this.btn_video.Click += new System.EventHandler(this.btn_video_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel6.ForeColor = System.Drawing.Color.White;
            this.panel6.Location = new System.Drawing.Point(1, 1);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(10, 37);
            this.panel6.TabIndex = 9;
            // 
            // btn_CPhoto
            // 
            this.btn_CPhoto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_CPhoto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_CPhoto.FlatAppearance.BorderSize = 0;
            this.btn_CPhoto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_CPhoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_CPhoto.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_CPhoto.ForeColor = System.Drawing.Color.White;
            this.btn_CPhoto.Location = new System.Drawing.Point(8, 1);
            this.btn_CPhoto.Margin = new System.Windows.Forms.Padding(1);
            this.btn_CPhoto.Name = "btn_CPhoto";
            this.btn_CPhoto.Size = new System.Drawing.Size(170, 37);
            this.btn_CPhoto.TabIndex = 8;
            this.btn_CPhoto.Text = "Foto";
            this.btn_CPhoto.UseVisualStyleBackColor = false;
            this.btn_CPhoto.Click += new System.EventHandler(this.btn_CPhoto_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel4.ForeColor = System.Drawing.Color.White;
            this.panel4.Location = new System.Drawing.Point(-6, 325);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(10, 37);
            this.panel4.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MV Boli", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(72, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 63);
            this.label2.TabIndex = 11;
            this.label2.Text = "DF";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(1, 325);
            this.button1.Margin = new System.Windows.Forms.Padding(1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(221, 37);
            this.button1.TabIndex = 6;
            this.button1.Text = "Crear";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MV Boli", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(44, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 31);
            this.label1.TabIndex = 10;
            this.label1.Text = "DeskFilter";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(-6, 273);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 37);
            this.panel3.TabIndex = 5;
            // 
            // btn_Detection
            // 
            this.btn_Detection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_Detection.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Detection.FlatAppearance.BorderSize = 0;
            this.btn_Detection.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_Detection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Detection.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_Detection.ForeColor = System.Drawing.Color.White;
            this.btn_Detection.Location = new System.Drawing.Point(1, 273);
            this.btn_Detection.Margin = new System.Windows.Forms.Padding(1);
            this.btn_Detection.Name = "btn_Detection";
            this.btn_Detection.Size = new System.Drawing.Size(221, 37);
            this.btn_Detection.TabIndex = 4;
            this.btn_Detection.Text = "Detección de movimiento";
            this.btn_Detection.UseVisualStyleBackColor = false;
            this.btn_Detection.Click += new System.EventHandler(this.btn_Detection_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(-6, 221);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 37);
            this.panel2.TabIndex = 3;
            // 
            // btn_off
            // 
            this.btn_off.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_off.Image = global::DeskFilter1._0.Properties.Resources.power_2_64__1_;
            this.btn_off.Location = new System.Drawing.Point(12, 561);
            this.btn_off.Name = "btn_off";
            this.btn_off.Size = new System.Drawing.Size(46, 47);
            this.btn_off.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_off.TabIndex = 2;
            this.btn_off.TabStop = false;
            this.btn_off.Click += new System.EventHandler(this.btn_off_Click);
            this.btn_off.MouseLeave += new System.EventHandler(this.btn_off_MouseLeave);
            this.btn_off.MouseHover += new System.EventHandler(this.btn_off_MouseHover);
            // 
            // btn_Photo
            // 
            this.btn_Photo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_Photo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Photo.FlatAppearance.BorderSize = 0;
            this.btn_Photo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_Photo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Photo.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_Photo.ForeColor = System.Drawing.Color.White;
            this.btn_Photo.Location = new System.Drawing.Point(1, 221);
            this.btn_Photo.Margin = new System.Windows.Forms.Padding(1);
            this.btn_Photo.Name = "btn_Photo";
            this.btn_Photo.Size = new System.Drawing.Size(220, 37);
            this.btn_Photo.TabIndex = 2;
            this.btn_Photo.Text = "Tomar foto";
            this.btn_Photo.UseVisualStyleBackColor = false;
            this.btn_Photo.Click += new System.EventHandler(this.btn_Photo_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(-6, 169);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(10, 37);
            this.panel1.TabIndex = 1;
            // 
            // btn_Home
            // 
            this.btn_Home.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.btn_Home.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Home.FlatAppearance.BorderSize = 0;
            this.btn_Home.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.btn_Home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Home.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btn_Home.ForeColor = System.Drawing.Color.White;
            this.btn_Home.Location = new System.Drawing.Point(1, 169);
            this.btn_Home.Margin = new System.Windows.Forms.Padding(1);
            this.btn_Home.Name = "btn_Home";
            this.btn_Home.Size = new System.Drawing.Size(221, 37);
            this.btn_Home.TabIndex = 0;
            this.btn_Home.Text = "Inicio";
            this.btn_Home.UseVisualStyleBackColor = false;
            this.btn_Home.Click += new System.EventHandler(this.button1_Click);
            // 
            // PanelContenido
            // 
            this.PanelContenido.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(85)))), ((int)(((byte)(169)))));
            this.PanelContenido.Controls.Add(this.PanelInfo);
            this.PanelContenido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelContenido.Location = new System.Drawing.Point(222, 30);
            this.PanelContenido.Name = "PanelContenido";
            this.PanelContenido.Size = new System.Drawing.Size(1078, 620);
            this.PanelContenido.TabIndex = 0;
            this.PanelContenido.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelContenido_Paint);
            // 
            // PanelInfo
            // 
            this.PanelInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(51)))), ((int)(((byte)(99)))));
            this.PanelInfo.Controls.Add(this.label4);
            this.PanelInfo.Controls.Add(this.linkLabel1);
            this.PanelInfo.Controls.Add(this.label3);
            this.PanelInfo.Location = new System.Drawing.Point(822, 0);
            this.PanelInfo.Name = "PanelInfo";
            this.PanelInfo.Size = new System.Drawing.Size(256, 265);
            this.PanelInfo.TabIndex = 0;
            this.PanelInfo.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(3, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(220, 105);
            this.label4.TabIndex = 2;
            this.label4.Text = "En caso de querer reportar un \r\nerror o una sugerencia puede \r\nenviar un correo a" +
    " :\r\nLuc2908@hotmail.com\r\n\r\n";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.linkLabel1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.linkLabel1.Location = new System.Drawing.Point(65, 122);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(135, 21);
            this.linkLabel1.TabIndex = 1;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Manual DeskFIlter";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(242, 84);
            this.label3.TabIndex = 0;
            this.label3.Text = "Si tiene alguna duda respecto al \r\nfuncionamiento de la aplicacion\r\n consulte el " +
    "manual de usuario en\r\n el enlace a continuación. \r\n";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 650);
            this.Controls.Add(this.PanelContenido);
            this.Controls.Add(this.PanelMenu);
            this.Controls.Add(this.BarraTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Main_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            this.BarraTitulo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_info)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Cerrar)).EndInit();
            this.PanelMenu.ResumeLayout(false);
            this.PanelMenu.PerformLayout();
            this.panelSubmenuC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_off)).EndInit();
            this.PanelContenido.ResumeLayout(false);
            this.PanelInfo.ResumeLayout(false);
            this.PanelInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel BarraTitulo;
        private System.Windows.Forms.Panel PanelMenu;
        private System.Windows.Forms.Panel PanelContenido;
        private System.Windows.Forms.PictureBox Btn_Cerrar;
        private System.Windows.Forms.PictureBox btn_info;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Home;
        private System.Windows.Forms.PictureBox btn_off;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_Detection;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_Photo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel PanelInfo;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelSubmenuC;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btn_video;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btn_CPhoto;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
    }
}

